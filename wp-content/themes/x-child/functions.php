<?php

// =============================================================================
// FUNCTIONS.PHP
// -----------------------------------------------------------------------------
// Overwrite or add your own custom functions to X in this file.
// =============================================================================

// =============================================================================
// TABLE OF CONTENTS
// -----------------------------------------------------------------------------
//   01. Enqueue Parent Stylesheet
//   02. Additional Functions
// =============================================================================

// Enqueue Parent Stylesheet
// =============================================================================

add_filter( 'x_enqueue_parent_stylesheet', '__return_true' );



// Additional Functions
// =============================================================================
add_filter( 'template_include', 'custom_single_product_template_include', 50, 1 );
function custom_single_product_template_include( $template ) {
	global $post;
    if ( is_singular('product') && (has_term( 'art-commissions-furniture-jewellery', 'product_cat')) ) {
        $template = get_stylesheet_directory() . '/woocommerce/single-product-mock-maker.php';
    }

  /*  if ( is_singular('product') && (has_term( 'art-commissions', 'product_cat')) ) {
        $template = get_stylesheet_directory() . '/woocommerce/single-product-mock-maker.php';
    }*/

	/* if ( is_singular('product') && (has_term( 'art-commissions', 'product_cat'))  && $post->ID == 37867) {
        $template = get_stylesheet_directory() . '/woocommerce/single-product-mock-maker.php';
    }*/
    return $template;
}

add_filter( 'woocommerce_variable_sale_price_html', 'woocommerce_remove_prices', 10, 3 );
add_filter( 'woocommerce_variable_price_html', 'woocommerce_remove_prices', 10, 3 );
add_filter( 'woocommerce_get_price_html', 'woocommerce_remove_prices', 10, 3 );
function woocommerce_remove_prices( $price, $product ) {
    if( is_product_category('maker-portfolio') || has_term( 'maker-portfolio', 'product_cat', $product->get_id() ) )
	{
        $price = '';
	}

    return $price;
}

add_filter( 'woocommerce_variable_sale_price_html', 'woocommerce_remove_pricess', 10, 2 );
add_filter( 'woocommerce_variable_price_html', 'woocommerce_remove_pricess', 10, 2 );
add_filter( 'woocommerce_get_price_html', 'woocommerce_remove_pricess', 10, 2 );
function woocommerce_remove_pricess( $price, $product ) {
    if( is_product_category('art-commissions-furniture-jewellery') || has_term( 'art-commissions-furniture-jewellery', 'product_cat', $product->get_id() ) )
        $price = '';

    return $price;
}


if ( ! function_exists( 'my_pagination' ) ) :
    function my_pagination() {
        global $wp_query;

        $big = 999999999; // need an unlikely integer

        echo paginate_links( array(
            'base' => str_replace( $big, '%#%', esc_url( get_pagenum_link( $big ) ) ),
            'format' => '?paged=%#%',
            'current' => max( 1, get_query_var('paged') ),
            'total' => $wp_query->max_num_pages
        ) );
    }
endif;

add_filter( 'template_include', 'var_template_include', 1000 );
function var_template_include( $t ){
    $GLOBALS['current_theme_template'] = basename($t);
    return $t;
}

function get_current_template( $echo = false ) {
    if( !isset( $GLOBALS['current_theme_template'] ) )
        return false;
    if( $echo )
        echo $GLOBALS['current_theme_template'];
    else
        return $GLOBALS['current_theme_template'];
}

add_action( 'wp_footer', 'add_pinterest_js' );
function add_pinterest_js(){
 ?>
<script async defer src="//assets.pinterest.com/js/pinit.js"></script>
<?php

}

add_filter( 'woocommerce_loop_add_to_cart_link', 'replacing_add_to_cart_button', 10, 2 );
function replacing_add_to_cart_button( $button, $product  ) {
	
	 if( is_product_category('art-commissions-furniture-jewellery') || has_term( 'art-commissions-furniture-jewellery', 'product_cat', $product->get_id() ) )
	{
    $button_text = __("Read More", "woocommerce");
    $button = '<a class="button" href="' . $product->get_permalink() . '">' . $button_text . '</a>';

    return $button;
	}
	else 
	{
		  return $button;
	}
}
/*add_action( 'wp_footer', 'zepto_script', 999999 );

function zepto_script () { 

//prevent it from executing within the builder to prevent more issues 
if ( ! isset( $_POST['cs_preview_state'] ) ) : ?>

<!--Zepto can be replaced with jQuery-->
<script text="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/zepto/1.2.0/zepto.min.js"></script>
<script type="text/javascript">
(function(d,s,id){var onLoad=function(){SB.Widget.initialize("kb",{siteUrl:"https://commissionit.supportbee.com/1574-commission-it-knowledge-base",iconColor:"#0494D0",textColor:"#FFFFFF"});};var js,fjs=d.getElementsByTagName(s)[0];if(d.getElementById(id)){return;}js=d.createElement(s);js.id=id;js.onload=onLoad;js.src="https://ddon89rxjwbw8.cloudfront.net/assets/sb_embeddable_hive.js";if (!fjs) document.body.append(js);else fjs.parentNode.insertBefore(js, fjs);}(document,"script","supportbee-jswidget"));
</script>

<?php endif;

}*/
//remove dead links
remove_action('wp_head', 'wp_shortlink_wp_head', 10, 0);
//disabld xmlrpc
//add_filter('xmlrpc_methods','__return_false');
function stop_pings ($vectors) {
unset( $vectors['pingback.ping'] );
return $vectors;
}
//add_filter( 'xmlrpc_methods', 'stop_pings');
add_filter('wp_headers', 'remove_pingback');

function remove_pingback( $headers )
{
unset($headers['X-Pingback']);
return $headers;
}
// Social Output
// =============================================================================

if ( ! function_exists( 'x_social_global' ) ) :
  function x_social_global() {

    $facebook    = x_get_option( 'x_social_facebook' );
    $twitter     = x_get_option( 'x_social_twitter' );
    $google_plus = x_get_option( 'x_social_googleplus' );
    $linkedin    = x_get_option( 'x_social_linkedin' );
    $xing        = x_get_option( 'x_social_xing' );
    $foursquare  = x_get_option( 'x_social_foursquare' );
    $youtube     = x_get_option( 'x_social_youtube' );
    $vimeo       = x_get_option( 'x_social_vimeo' );
    $instagram   = x_get_option( 'x_social_instagram' );
    $pinterest   = x_get_option( 'x_social_pinterest' );
    $dribbble    = x_get_option( 'x_social_dribbble' );
    $flickr      = x_get_option( 'x_social_flickr' );
    $github      = x_get_option( 'x_social_github' );
    $behance     = x_get_option( 'x_social_behance' );
    $tumblr      = x_get_option( 'x_social_tumblr' );
    $whatsapp    = x_get_option( 'x_social_whatsapp' );
    $soundcloud  = x_get_option( 'x_social_soundcloud' );
    $rss         = x_get_option( 'x_social_rss' );

    $output = '<div class="x-social-global">';

      if ( $facebook )    : $output .= '<a href="' . $facebook    . '" rel="nofollow" class="facebook" title="Facebook" target="_blank"><i class="x-icon-facebook-square" data-x-icon-b="&#xf082;" aria-hidden="true"></i></a>'; endif;
      if ( $twitter )     : $output .= '<a href="' . $twitter     . '" rel="nofollow" class="twitter" title="Twitter" target="_blank"><i class="x-icon-twitter-square" data-x-icon-b="&#xf081;" aria-hidden="true"></i></a>'; endif;
      if ( $google_plus ) : $output .= '<a href="' . $google_plus . '" rel="nofollow" class="google-plus" title="Google+" target="_blank"><i class="x-icon-google-plus-square" data-x-icon-b="&#xf0d4;" aria-hidden="true"></i></a>'; endif;
      if ( $linkedin )    : $output .= '<a href="' . $linkedin    . '" rel="nofollow" class="linkedin" title="LinkedIn" target="_blank"><i class="x-icon-linkedin-square" data-x-icon-b="&#xf08c;" aria-hidden="true"></i></a>'; endif;
      if ( $xing )        : $output .= '<a href="' . $xing        . '" rel="nofollow" class="xing" title="XING" target="_blank"><i class="x-icon-xing-square" data-x-icon-b="&#xf169;" aria-hidden="true"></i></a>'; endif;
      if ( $foursquare )  : $output .= '<a href="' . $foursquare  . '" rel="nofollow" class="foursquare" title="Foursquare" target="_blank"><i class="x-icon-foursquare" data-x-icon-b="&#xf180;" aria-hidden="true"></i></a>'; endif;
      if ( $youtube )     : $output .= '<a href="' . $youtube     . '" rel="nofollow" class="youtube" title="YouTube" target="_blank"><i class="x-icon-youtube-square" data-x-icon-b="&#xf431;" aria-hidden="true"></i></a>'; endif;
      if ( $vimeo )       : $output .= '<a href="' . $vimeo       . '" rel="nofollow" class="vimeo" title="Vimeo" target="_blank"><i class="x-icon-vimeo-square" data-x-icon-b="&#xf194;" aria-hidden="true"></i></a>'; endif;
      if ( $instagram )   : $output .= '<a href="' . $instagram   . '" rel="nofollow" class="instagram" title="Instagram" target="_blank"><i class="x-icon-instagram" data-x-icon-b="&#xf16d;" aria-hidden="true"></i></a>'; endif;
      if ( $pinterest )   : $output .= '<a href="' . $pinterest   . '" rel="nofollow" class="pinterest" title="Pinterest" target="_blank"><i class="x-icon-pinterest-square" data-x-icon-b="&#xf0d3;" aria-hidden="true"></i></a>'; endif;
      if ( $dribbble )    : $output .= '<a href="' . $dribbble    . '" rel="nofollow" class="dribbble" title="Dribbble" target="_blank"><i class="x-icon-dribbble" data-x-icon-b="&#xf17d;" aria-hidden="true"></i></a>'; endif;
      if ( $flickr )      : $output .= '<a href="' . $flickr      . '" rel="nofollow" class="flickr" title="Flickr" target="_blank"><i class="x-icon-flickr" data-x-icon-b="&#xf16e;" aria-hidden="true"></i></a>'; endif;
      if ( $github )      : $output .= '<a href="' . $github      . '" rel="nofollow" class="github" title="GitHub" target="_blank"><i class="x-icon-github-square" data-x-icon-b="&#xf092;" aria-hidden="true"></i></a>'; endif;
      if ( $behance )     : $output .= '<a href="' . $behance     . '" rel="nofollow" class="behance" title="Behance" target="_blank"><i class="x-icon-behance-square" data-x-icon-b="&#xf1b5;" aria-hidden="true"></i></a>'; endif;
      if ( $tumblr )      : $output .= '<a href="' . $tumblr      . '" rel="nofollow" class="tumblr" title="Tumblr" target="_blank"><i class="x-icon-tumblr-square" data-x-icon-b="&#xf174;" aria-hidden="true"></i></a>'; endif;
      if ( $whatsapp )    : $output .= '<a href="' . $whatsapp    . '" rel="nofollow" class="whatsapp" title="Whatsapp" target="_blank"><i class="x-icon-whatsapp" data-x-icon-b="&#xf232;" aria-hidden="true"></i></a>'; endif;
      if ( $soundcloud )  : $output .= '<a href="' . $soundcloud  . '" rel="nofollow" class="soundcloud" title="SoundCloud" target="_blank"><i class="x-icon-soundcloud" data-x-icon-b="&#xf1be;" aria-hidden="true"></i></a>'; endif;
      if ( $rss )         : $output .= '<a href="' . $rss         . '" rel="nofollow" class="rss" title="RSS" target="_blank"><i class="x-icon-rss-square" data-x-icon-s="&#xf143;" aria-hidden="true"></i></a>'; endif;

    $output .= '</div>';

    echo $output;

  }
endif;




add_filter( 'style_loader_src',  'sdt_remove_ver_css_js', 9999, 2 );
add_filter( 'script_loader_src', 'sdt_remove_ver_css_js', 9999, 2 );
function sdt_remove_ver_css_js( $src, $handle ) 
{
    $handles_with_version = [ 'style' ]; // <-- Adjust to your needs!

    if ( strpos( $src, 'ver=' ) && ! in_array( $handle, $handles_with_version, true ) )
        $src = remove_query_arg( 'ver', $src );

    return $src;
} 



add_action( 'caldera_forms_submit_complete',function( $form, $referrer, $process_id, $entryid ){
  
  //change form ID to match your form
  if( 'CF5eb39ee002741' === $form[ 'ID' ] ){
    //change field to match field used with processor
    $field_id = 'fld_71456';

    //get saved field value
    $value = Caldera_Forms::get_field_data( $field_id, $form, $entryid );
    $main_data = array();

    $formdata = $form['fields']['fld_71456']['config']['option'];
    foreach($formdata as $fdata){
      $valuedata =  $fdata['value'];
      if (in_array($valuedata, $value)){
      	$main_data[] = $fdata['label'];

      }
    }
    $final_array = implode("",$main_data);
    if( $value ){
    	$value = $final_array;
      Caldera_Forms::set_field_data( $field_id, $value, $form, $entryid );
    }

  }

}, 5, 4); 



add_filter( 'wpcf7_mail_components', 'mycustom_wpcf7_mail_components', 50, 2 );
function mycustom_wpcf7_mail_components($components, $form = null) {

  $current_form_id = $form->id;

  if($current_form_id == 60343){
  global $session;
    session_start();
    $sketch_session_data = $_SESSION["sketch"];
    $new_path = get_home_path();
    //$components['attachments'][] = '/home/customer/www/commissionit.com/public_html/wp-content/themes/x-child/sketch-tool/drawings/n3vdhr6cuk.png';
    $components['attachments'][] = $new_path.'wp-content/themes/x-child/sketch-tool/drawings/'.$sketch_session_data.'.png';
    return $components;

  }else{
    return $components;
  }

}
//remove_action( 'woocommerce_archive_description', 'woocommerce_taxonomy_archive_description', 10 );
//add_action( 'woocommerce_after_shop_loop', 'woocommerce_taxonomy_archive_description', 100 );



add_action( 'woocommerce_after_shop_loop' , 'add_lower_description_category_page', 5 );
function add_lower_description_category_page() {
  if(is_product_category()){
      $category = get_queried_object();
      //echo $category->term_id; 
      $lower_description = get_field( "lower_description", $category );
      if( $lower_description ) {
        $course_description = nl2br($lower_description);
      $course_description = trim($course_description);
          echo '<div class="lower_description">'.$course_description.'</div>';
      } 
  }
}

/********************* Include Tag in wooOCOmmerce Search ****************/


//add_filter( 'posts_search', 'woocommerce_search_product_tag_extended', 10, 2 );
function woocommerce_search_product_tag_extended( $search, $query ) {
    global $wpdb, $wp;

    $qvars = $wp->query_vars;

    if ( is_admin() || empty($search) ||  ! ( isset($qvars['s'])
    && isset($qvars['post_type']) && ! empty($qvars['s'])
    && $qvars['post_type'] === 'product' ) ) {
        return $search;
    }
    
    // Here set your custom taxonomy
    $taxonomy = 'product_tag'; // WooCommerce product tag

    // Get the product Ids
    $ids = get_posts( array(
        'posts_per_page'  => -1,
        'post_type'       => 'product',
        'post_status'     => 'publish',
        'fields'          => 'ids',
        'tax_query'       => array( array(
            'taxonomy' => $taxonomy,
            'field'    => 'name',
            'terms'    => esc_attr($qvars['s']),
        )),
    ));

    if ( count( $ids ) > 0 ) {
        $search = str_replace( 'AND (((', "AND ((({$wpdb->posts}.ID IN (" . implode( ',', $ids ) . ")) OR (", $search);
    }
    return $search;
}


add_action( 'woocommerce_before_add_to_cart_form', 'add_text_single_product', 10 );
function add_text_single_product(){
	global $product;
    $id = $product->get_id();
	
	$prod_terms = get_the_terms( $id, 'product_cat' );
	foreach ($prod_terms as $prod_term) {
         $product_cat_id = $prod_term->term_id;
         // gets an array of all parent category levels
        $product_parent_categories_all_hierachy = get_ancestors( $product_cat_id, 'product_cat' );  
       // This cuts the array and extracts the last set in the array
       $last_parent_cat = array_slice($product_parent_categories_all_hierachy, -1, 1, true);
       foreach($last_parent_cat as $last_parent_cat_value){
           
		   if($last_parent_cat_value == '618')
		   {
			   echo "<p style='font-size:18px;'><strong>FREE UK DELIVERY</strong></p>";	
		   }
       }

    }
	
}
add_action( 'woocommerce_before_single_product_summary', 'add_bredcrum', 10 );
function add_bredcrum(){
	woocommerce_breadcrumb();
	
}

remove_action( 'woocommerce_after_shop_loop_item_title', 'woocommerce_template_loop_price', 10 );


 add_action( 'woocommerce_loaded', 'update_products_by_x' );
function update_products_by_x(){
    $limit = 200;

    // getting all products
    $products_ids = get_posts( array(
        'post_type'        => 'product', // or ['product','product_variation'],
        'numberposts'      => $limit,
        'post_status'      => 'publish',
        'fields'           => 'ids',
        'meta_query'       => array( array(
            'key'     => '_sync_updated',
            'compare' => 'NOT EXISTS',
        ) )
    ) );

    // Loop through product Ids
    foreach ( $products_ids as $product_id ) {

        // Get the WC_Product object
        $product = wc_get_product($product_id);

        // Mark product as updated
        $product->update_meta_data( '_sync_updated', true );

        $product->save();
    }
}

add_shortcode( 'update_title', 'wpdocs_footag_func' );
function wpdocs_footag_func() {
    
	$args = array(
            'post_type' => 'product',
            'post_status' => array('publish'),
            'posts_per_page' => 10,
            'orderby' => 'title',
            'order'   => 'ASC',
			'product_cat'    => 'abstract-modernism'
        );
        $loop = new WP_Query($args);
        //print_r($loop); die();
        while ($loop->have_posts()):
            $loop->the_post();
            $product_id = (int) $loop->post->ID;
			$product_title = $loop->post->post_title;
			 echo "<pre>";
			   print_r($product_title);
			   echo "</pre>"; 
			//echo $product_id;
			$my_post = array(
					'ID'           => $product_id,
					'post_title'   => $product_title .' Oil Painting Reproductions',
      
				);
           // wp_update_post( $my_post );
        endwhile;
   /* echo "<pre>";
   print_r($loop);
   echo "</pre>"; */
	
}

//**********************  Work DONE By Irfan Ahmed 2021 June 22 */

if( function_exists('acf_add_options_page') ) {
	
	//acf_add_options_page();
	
}

// add_action( 'woocommerce_product_query', 'bbloomer_hide_products_category_shop' );
   
// function bbloomer_hide_products_category_shop( $q ) {
  
//     $tax_query = (array) $q->get( 'tax_query' );
  
//     $tax_query[] = array(
//            'taxonomy' => 'product_cat',
//            'field' => 'id',
//            'terms' => array( 618 ), // Category slug here
//            'operator' => 'NOT IN'
//     );
  
  
//     $q->set( 'tax_query', $tax_query );
  
// }


add_shortcode( 'update_sku', 'wpdocs_footag_func_sku' );
function wpdocs_footag_func_sku() {
    
	$args = array(
            'post_type' => 'product',
            'post_status' => array('publish'),
            'posts_per_page' => 900,
            'orderby' => 'title',
            'order'   => 'ASC',
			'paged' => 1,   
			'product_cat'    => 'still-lifes'
        );
        $loop = new WP_Query($args);
        //print_r($loop); die();
        while ($loop->have_posts()):
            $data = $loop->the_post();
            $product_id = (int) $loop->post->ID;
			$sku = get_post_meta( $product_id , '_sku', true );
			$expload = explode("-",$sku);   
			   //update_post_meta( $product_id, '_sku', 'oil-paint-reproductions-'.$expload[1] );
			 echo "<pre>";
			   print_r($sku);
			   echo "</pre>"; 
		
        endwhile;

	
}
function wcfm_custom_0905_translate_text( $translated ) {
	$translated = str_ireplace( 'Become a Vendor', 'Register as a Artist/Maker', $translated );
	return $translated;     
}
add_filter('gettext', 'wcfm_custom_0905_translate_text');
add_filter('ngettext', 'wcfm_custom_0905_translate_text');
add_filter( 'woocommerce_product_tabs', 'remove_additional_information_tab', 100, 1 );
function remove_additional_information_tab( $tabs ) {
    unset($tabs['additional_information']);

    return $tabs;
}


// Add "additional information" after add to cart
add_action( 'woocommerce_single_product_summary', 'additional_info_under_add_to_cart', 35 );
function additional_info_under_add_to_cart() {
    global $product;

    if ( $product && ( $product->has_attributes() || apply_filters( 'wc_product_enable_dimensions_display', $product->has_weight() || $product->has_dimensions() ) ) ) {
        wc_display_product_attributes( $product );
    }
}
// Function to exclude certain categories from your WordPress RSS feed
// add_filter( 'get_terms', 'ts_get_subcategory_terms', 10, 3 );
// function ts_get_subcategory_terms( $terms, $taxonomies, $args ) {
// $new_terms = array();
// // if it is a product category and on the shop page
// if ( in_array( 'product_cat', $taxonomies ) && ! is_admin() &&is_shop() ) {
// foreach( $terms as $key => $term ) {
// if ( !in_array( $term->slug, array( 'digital','digital-art-art' ) ) ) { //pass the slug name here
// $new_terms[] = $term;
// }}
// $terms = $new_terms;
// }
// return $terms;
// }


// add_action( 'woocommerce_product_query', 'prefix_custom_pre_get_posts_query' );
// /**
// * Hide Product Categories from targeted pages in WooCommerce
// * @link https://gist.github.com/stuartduff/bd149e81d80291a16d4d3968e68eb9f8#file-wc-exclude-product-category-from-shop-page-php
// *
// */
// function prefix_custom_pre_get_posts_query( $q ) {

// if( is_shop() || is_page('shopping-products') ) { // set conditions here

// $tax_query = (array) $q->get( 'tax_query' );

// $tax_query[] = array(
// 'taxonomy' => 'digital',
// 'field' => 'slug',
// 'operator' => 'NOT IN'
// );

// $q->set( 'tax_query', $tax_query );
// }
// }