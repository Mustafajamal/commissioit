<?php 

get_header('shop');

$terms = get_terms([
  'taxonomy' => 'product_cat',
  'hide_empty' => false,
]);
  
  foreach ($terms as $term) {
    if($term->slug != 'sculptures' && $term->slug != 'wood-works' && $term->slug != 'water_works' && $term->slug != 'sign-making' && $term->slug != 'Sculptures' && $term->slug != 'rugs-upholstery' && $term->slug != 'recycled-upcycled-works' && $term->slug != 'mosaic-tile-artists' && $term->slug != 'model-making' && $term->slug != 'metalwork-fabrication-laser-cutting-service-metal-gates' && $term->slug != 'lighting' && $term->slug != 'leather-works' && $term->slug != 'landscape-gardening' && $term->slug != 'kitchens' && $term->slug != 'jewellery' && $term->slug != 'art-commissions-furniture-jewellery' && $term->slug != 'digital' && $term->slug != 'flowers' && $term->slug != 'animals' && $term->slug != 'graffiti_murals' && $term->slug != 'installation-art' && $term->slug != 'landscape' && $term->slug != 'oil-painting' && $term->slug != 'abstract-modernism' && $term->slug != 'figures' && $term->slug != 'impressionism' && $term->slug != 'impressionism-figures' && $term->slug != 'old-masters' && $term->slug != 'still-lifes' && $term->slug != 'art-commissions-commission-an-artist' && $term->slug != 'abstract-art' && $term->slug != 'landscape-2' && $term->slug != 'pencil_drawings' && $term->slug != 'pet_portraits_from_photo' && $term->slug != 'portraits' && $term->slug != 'portraits-from-photos' && $term->slug != 'bespoke-furniture-wardrobes-tables-desks-shelves' && $term->slug != 'parametric_cnc_manufacturing_3d_printing_furniture_art' && $term->slug != 'graphic-design-art-design-logo-design-3d-cad-design-video-production' && $term->slug != 'glass_works' && $term->slug != 'handmade_fashion-jackets_jumpers_hats_dresses' && $term->slug != 'interior-design') { 
      $slug.= $term->term_id.','; 
    }
  }
  
?>
<div class="x-container max width offset"> 
<?php  echo do_shortcode ( '[product_categories ids='.$slug.']'); ?>
</div>
<?php get_footer(); ?>

  ?>