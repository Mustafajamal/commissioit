<?php

get_header();

?>

<div class="x-container max width offset">
    <div class="x-main full" role="main">
        <style>
		a#view-prdct {
    margin-top: 10px;
}
		
		.cross-sells {
   
    padding-bottom: 50px;
}
            .market_details, .custom_portfolio_row {
                width: 100%;
                float: left;
                position: relative;
            }
            .market_details {
                margin-top: 13px;
            }
            .custom_portfolio_row .big_image img {
                width: 100%;
                height: 560px;
                object-fit: cover;
            }
            .custom_portfolio_row .big_image {
                width: 45%;
                float: left;
                margin-right: 10px;
            }
            .custom_portfolio_row .small_images {
                width: 20%;
                float: left;
                margin-right: 20px;

            }
            .custom_portfolio_row .small_images .small_image.active {
                display: none;
            }
            .custom_portfolio_row .small_images .small_image.active img {
                border: 4px solid #e86c3f;
            }
            .custom_portfolio_row .small_images img {
                width: 180px;
                height: 180px;
                object-fit: cover;
                margin-bottom: 10px;
                cursor:pointer;
            }
            .custom_portfolio_row .text_content {
                width: calc(35% - 40px);
                float: left;
                text-align: center;
            }
            .custom_portfolio_row .text_content .content {
                display: none;
            }
            .custom_portfolio_row .text_content .content.active {
                display: block;
            }
            .custom_portfolio_row .text_content p, .market_details p {
                font-size: 1.3em;
                font-style: normal;
                font-weight: 400;
                line-height: 1.6;
                letter-spacing: 0em;
                text-transform: none;
                color: #888888;
            }
            .custom_portfolio_row .text_content h3,
            .market_details h3 {
                margin: 0 0 10px;
                font-weight: 500;
            }
            a.how_we_work {
                width: 180px;
                height: 45px;
                display: block;
                background-color: #D7D8DA;
                border-radius: 8px;
                font-family: inherit;
                font-size: 17px;
                font-style: normal;
                font-weight: 400;
                line-height: 1;
                text-align: center;
                color: black;
                padding: 12px 0;
                box-shadow: 0em 0.15em 0.65em 0em rgba(0,0,0,0.25);
                margin: 0 auto;
                transition: .7s;
                margin-bottom: 15px;
            }
            a.how_we_work:hover {
                background-color: #ffffff;
            }
            a.launch_project {
                width: 180px;
                height: 45px;
                display: block;
                background-color: #E86C3F;
                border-radius: 8px;
                font-family: inherit;
                font-size: 17px;
                font-style: normal;
                font-weight: 400;
                line-height: 1;
                text-align: center;
                color: white;
                padding: 12px 0;
                box-shadow: 0em 0.15em 0.65em 0em rgba(0,0,0,0.25);
                margin: 0 auto;
                transition: .7s;
                border-bottom: 4px solid #e3934a;
            }
            a.launch_project:hover{

                background-color: #d35f3b;

            }
            .custom_share {
                margin-top: 0;
            }
            .custom_share a {
                color:#646464;
            }
            .custom_share a:hover{
                color:#444444!important;
                background-color: transparent!important;
            }
            .custom_share p {
                font-size: 15px!important;
            }
            article h1 {
                font-weight: 500;
                font-size: 38px;
                margin-top: 0;
                margin-bottom: 20px;
            }
            article {
                margin-top: 0!important;
            }

            a.mood_board {
                color: black;
                width: 166px;
                margin: 0 auto;
                background: white;
                display: inline-block;
                margin-bottom: -31px;
            }
            a.mood_board span, a.mood_board img {
                display: inline-block;
                float: left;
            }
            a.mood_board span {
                padding-top: 4px;
                padding-left: 4px;
            }

            @media (max-width:992px){
                .custom_portfolio_row .big_image {
                    width: 100%;
                    max-width: 600px;
                    float: left;
                    margin-right: 0;
                }
                .custom_portfolio_row .slider{
                    width: 100%;
                    max-width: 600px;
                }
                .custom_portfolio_row .small_images {
                    width: 100%;
                    max-width: 600px;
                    float: left;
                    margin-right: 0;
                }
                .custom_portfolio_row .small_images .small_image {
                    float: left;
                    margin-right: 10px;
                    margin-top:10px;
                }

                .custom_portfolio_row .small_images img {
                    width: 180px;
                    height: 180px;
                    object-fit: cover;
                    margin-bottom: 10px;
                    cursor:pointer;
                }
                .custom_portfolio_row .text_content {
                    width: 100%;
                    max-width: 600px;
                    float: left;
                    text-align: center;
                    margin-top:20px;
                    position: relative;
                }
                .custom_portfolio_row {
                    display: flex;
                    justify-content: center;
                }
            }
            @media (max-width:480px){
                .custom_portfolio_row .small_images img {
                    width: 100%;
                    height: 100px;
                }
                .custom_portfolio_row .small_images > div {
                    width: calc(33% - 6px);
                }
                .custom_portfolio_row .small_images > div:last-child {
                    margin-right: 0;
                }
                .custom_portfolio_row .big_image img {
                    width: 100%;
                    height: 300px;
                    object-fit: cover;
                }

            }
            .next_post_link a {
                display: inline-block;
                width: 40px;
                height: 40px;
                background: white;
                border-radius: 4px;
                text-align: center;
                font-size: 23px;
                color: #333333;
                box-shadow: 0em 0em 0.55em 0em rgba(0,0,0,0.25);
            }
            .next_post_link {
                position: absolute;
                right: -45px;
                top:38%;
            }
        </style>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-zoom/1.7.21/jquery.zoom.min.js"></script>
        <script>
            jQuery(document).ready(function ($) {
                var img = $('.big_image img').attr('src');
                $('.text_content .content').eq(0).addClass('active');
                $('.small_images .small_image').eq(0).addClass('active');

                $('.small_images .small_image').click(function () {
                    $('.small_images .small_image').removeClass('active');
                    $('.text_content .content').removeClass('active');
                    $(this).addClass('active');
                    img = $(this).children().attr('src');
                    $('.big_image img').attr('src', img);
                    $('.text_content .content').eq($(this).data('id')).addClass('active');

                    $('.big_image').trigger('zoom.destroy'); // remove zoom
                    $('.big_image').zoom({url: img, magnify: 2});
                });

                $('.big_image').zoom({url: img,
                    magnify: 2});


            });
        </script>
        <?php while ( have_posts() ) : the_post(); ?>

            <article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
                <h1><?php the_title(); ?></h1>
                <div class="custom_portfolio_row">
                    
                    <?php
					$pid = get_the_ID();
                        //$portfolio = get_field('portfolio',$pid);
						//echo "<pre>";
						//print_r($portfolio);
						$image1 = get_field('image',$pid);
						$image2 = get_field('image_2',$pid);
						$image3 = get_field('image_3',$pid);
						$image4 = get_field('image_4',$pid);
						//$image4 = get_field('image_5',$pid);
                    ?>
                    <div class="slider">
                        <div class="big_image">
                            <img src="<?php echo $image1;//echo $portfolio[0]['image'] ?>" alt="">
                        </div>
                        <div class="small_images">
						<div class="small_image" data-id="<?php echo $image_i; ?>">
                                 <img src="<?php echo $image1;//the_sub_field('image'); ?>" alt="">
                        </div>
						<div class="small_image" data-id="<?php echo $image_i; ?>">
                                 <img src="<?php echo $image2;//the_sub_field('image'); ?>" alt="">
                        </div>
						<div class="small_image" data-id="<?php echo $image_i; ?>">
                                 <img src="<?php echo $image3;//the_sub_field('image'); ?>" alt="">
                        </div>
						<div class="small_image" data-id="<?php echo $image_i; ?>">
                                 <img src="<?php echo $image4;//the_sub_field('image'); ?>" alt="">
                        </div>
						
                            <?php
                                if( have_rows('portfolio') ):
                                    $image_i = 0;
                                  
									while ( have_rows('portfolio') ) : the_row();
/*
                                        ?>
                                        <div class="small_image" data-id="<?php echo $image_i; ?>">
                                            <img src="<?php the_sub_field('image'); ?>" alt="">
                                        </div>
                                <?php*/
                                        $image_i++;
                                    endwhile;

                                    endif;
                            ?>
                        </div>
                        <div class="text_content">
                            <h3 class="x-text-content-text-primary">Project Details</h3>
                            <?php
                            if( have_rows('portfolio') ):
                                $content_i = 0;
                                while ( have_rows('portfolio') ) : the_row();

                                    ?>
                                    <div class="content" data-id="<?php echo $content_i; ?>">
                                        <p><?php //the_sub_field('description'); ?></p>

                                        <a href="/how-it-works/" class="how_we_work">How we work!</a>
<?php if( $product->is_type( 'simple_portfolio' ) ){?>
										
										  <a class="popmake-get-quote launch_project" >Get Quote</a>
										 
										   
											
<?php } ?>

<?php 	
	 $crosssells = get_post_meta( get_the_ID(), '_crosssell_ids',true);
	 
	  if(!empty($crosssells)){ ?>
	  
	   <!--  <a class="launch_project" href="#show-products">View Products</a> -->
		<a class="launch_project" id="view-prdct" href="#show-products">Select Options</a>
		  
		  <?php } ?>

                                    </div>

                                    <?php
                                    $content_i++;
                                endwhile;

                            endif;
                            ?>
                            <div class="custom_share">
                                <?php x_portfolio_item_social(); ?>
                            </div>
                            <a class="mood_board" href="https://app.commissionit.com/bingsearch" target="_blank" style="outline: none;"><img src="https://www.commissionit.co.uk/wp-content/uploads/2018/10/mood-board.png" alt="Create Mood Board"><span>Create Mood Board</span></a>


                        </div>
                    </div>
                </div>
                <div class="market_details">
                    <h3>Maker Details</h3>
                    <?php the_content(); ?>
					
					<?php 	
	 $crosssells = get_post_meta( get_the_ID(), '_crosssell_ids',true);
	 
	  if(!empty($crosssells)){
       
    

     $args = array( 
        'post_type' => 'product', 
        'posts_per_page' => 4, 
        'post__in' => $crosssells 
        );
    $products = new WP_Query( $args );
    if( $products->have_posts() ) : 
        echo '<div id="show-products" class="cross-sells"><h2>Products on sale</h2>';
        woocommerce_product_loop_start();
        while ( $products->have_posts() ) : $products->the_post();
            wc_get_template_part( 'content', 'product' );
        endwhile; // end of the loop.
        woocommerce_product_loop_end();
        echo '</div>';
    endif;
    wp_reset_postdata();
	}
	
			
				
				
				?>
                </div>
				
            </article>
        <?php endwhile; ?>

    </div>
</div>
<script>jQuery( ".get-qte-btn a" ).addClass( "launch_project" );
 jQuery( "#AnythingPopup_BoxClose1 a" ).removeClass( "launch_project" );

</script>

<?php get_footer(); ?>