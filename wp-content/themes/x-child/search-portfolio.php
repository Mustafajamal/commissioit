<?php 
/*
Template Name:Search-portfolio
*/
 get_header(); 
 ?>
 <?php if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>

<?php 
	$keyword= $_GET["pr"];	

$args = array(
 's' => $keyword,
'post_type' => 'product',
'product_cat' => 'bespoke-suits-furniture-jewellery-kitchens',
'posts_per_page' => 50, 
 

);
							 
$the_query = new WP_Query( $args ); 
$posts = $the_query->posts;

?> 

 <div class="x-container max width offset">
    <section id="primary" class="content-area">
        <main id="main" class="site-main" role="main">
       <div class="x-main left" role="main">
	 
	   <div class="portfolio-makers">
 <h2>Showing All Products:</h2>	

	     <div class="x-row search">
		 <?php					 
					 foreach($posts as $wills)
	{
		 $image = wp_get_attachment_image_src(get_post_thumbnail_id($wills->ID), 'thumbnail');
 ?>
				
					<div class="col-sm-3">
			<div class="entry-product">
			<div class="entry-featured">
			<a href="<?php echo get_permalink($wills->ID);?>"><img src="<?php echo $image[0]; ?>" class="attachment-shop_catalog size-shop_catalog wp-post-image" /></a></div>
            <div class="product-info">
			<div class="product-details">
			<h3><a href="<?php echo get_permalink($wills->ID);?>"><?php echo $wills->post_title; ?></a></h3>
			<?php  $sale_price = get_post_meta( $wills->ID, '_price', true); ?>
			<?php if($sale_price){ ?>
			<span class="price"><span class="woocommerce-Price-amount amount">			
             <span class="woocommerce-Price-currencySymbol"><?php global  $woocommerce;   echo get_woocommerce_currency_symbol(); ?></span><?php echo $sale_price; ?></span></span> <?php			 
			 } 
			 ?>		
			
			 </div>
			</div>
			</div>
			</div>	
<?php }?>				
				
</div>

</div>


 </div>
		
		<aside class="x-sidebar right po" role="complementary">
		<?php dynamic_sidebar( 'main-sidebar' ); ?>
		</aside>
		
        </main><!-- #main -->
    </section><!-- #primary -->
</div>
<?php endwhile; 
				else : ?>
	<p><?php esc_html_e( 'Sorry, no posts matched your criteria.' ); ?></p>
<?php  endif; ?>
 
 
 <?php get_footer(); ?>