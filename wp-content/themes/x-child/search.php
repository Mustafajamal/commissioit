<?php
/**
 * The template for displaying search results pages.
 *
 * @package stackstar.
 */
 
get_header(); ?>
<style>
span.p-key {
    text-transform: capitalize;
}
</style>

    <div class="x-container max width offset">
    <section id="primary" class="content-area">
        <main id="main" class="site-main" role="main">
       <div class="x-main left" role="main">
	 
<div class="portfolio-makers">
    <div class="x-row search">
		
	  <?php
	$keywordd= $_GET["s"];
	$types = new WP_Query( "post_type=product&post_status=publish&posts_per_page=-1&s=$keywordd&order=ASC&product_cat=bespoke-suits-furniture-jewellery-kitchens" );
	
	$posts = $types->posts;
	
	
	$countpost = count($posts);
       if( $countpost > 0){  
	   ?>
	    <h2>Portfolio Makers Result</h2>
		<?php
	
	
$i = 0;
    foreach( $posts as $type ){
		
		
               
                $imagee = wp_get_attachment_image_src( get_post_thumbnail_id( $type->ID ));
			   ?>
			  

					<div class="col-sm-3">
					
			<div class="entry-product">
			<div class="entry-featured">
			<a href="<?php echo get_permalink($type->ID);?>"><img src="<?php echo get_the_post_thumbnail_url($type->ID, 'thumbnail'); ?>" class="attachment-shop_catalog size-shop_catalog wp-post-image" style="width:236px; height:236px;" /></a></div>
            <div class="product-info">
			<div class="product-details">
			<h3><a href="<?php echo get_permalink($type->ID);?>"><?php echo $type->post_title; ?></a></h3>
			
			
			 </div>
			</div>
			</div>
			</div>				
				<?php
				if ($i++ == 2) break; 
            }
			
	   }

?>

</div>



<?php if($countpost >3){?>
<div class='show-all'><a target="_blank" href="<?php echo site_url(); ?>/portfolio-products/?pr=<?php echo $search_query = get_search_query(); ?>">Show 3 of <?php echo count($posts); ?> Results</a></div>
<?php } ?>


</div>



<!--- <div class="products-list">
	     <div class="x-row search">
	   <?php
	$keyword= $_GET["s"];
	//$typess = new WP_Query( "post_type=product&category__not_in=bespoke-suits-furniture-jewellery-kitchens&post_status=publish&s=$keyword&order=ASC" );
	
	$typess = new WP_Query( "post_type=product&category__not_in=bespoke-suits-furniture-jewellery-kitchens&post_status=publish&posts_per_page=-1&s=$keyword&order=ASC" );
	
	$postss = $typess->posts;
	
	 $countpostt = count($postss);
       if( $countpostt > 0){  
	?>
	   <h2>Shopping Products Result</h2>
	   <?php
	
$j = 0;
    foreach( $postss as $typee ){
                $image = wp_get_attachment_image_src( get_post_thumbnail_id( $typee->ID ));
			   ?>
			<div class="col-sm-3">
			<div class="entry-product">
			<div class="entry-featured">
			<a href="<?php echo $typee->guid; ?>"><img src="<?php echo get_the_post_thumbnail_url($typee->ID, 'thumbnail'); ?>" class="attachment-shop_catalog size-shop_catalog wp-post-image" /></a></div>
            <div class="product-info">
			<div class="product-details">
			<h3><a href="<?php echo get_permalink($typee->ID); ?>"><?php echo $typee->post_title; ?></a></h3>
			<?php  $sale_price = get_post_meta( $typee->ID, '_price', true); ?>
			<?php if($sale_price){ ?>
			<span class="price"><span class="woocommerce-Price-amount amount">			
             <span class="woocommerce-Price-currencySymbol"><?php global  $woocommerce;   echo get_woocommerce_currency_symbol(); ?></span><?php echo $sale_price; ?></span></span> <?php			 
			 } 
			 ?>
			  <div class="woo-buttn">
			 <?php
			 woocommerce_template_loop_add_to_cart($typee->ID);
			 
			 ?>
			 </div>			 
			 
			 </div>
			</div>
			</div>
			</div>				
				<?php
          
            	if ($j++ == 2) break;
      
    }
	?>
	<?php
}//else{echo 'No Shopping Products Found';}
 
?>

 
</div>
<?php if($countpostt >3){?>

<div class='show-all'><a target="_blank" href="<?php echo site_url(); ?>/shopping-products/?sr=<?php echo $search_query = get_search_query(); ?>">Show 3 of <?php echo count($postss); ?> Results</a></div>
<?php } ?> 
</div> --->



<div class="bottom-section-search">

<?php
 if( $countpostt <= 0 && $countpost <= 0){ ?>
		<div class="top-img-section">
		
			<p>Want to make a bespoke <span class="p-key">"<?php echo $search_query = get_search_query(); ?>"</span>, create an Image Board to get started.<a title="Know how it works" target="_blank" href="/how-it-works"><img src="/wp-content/uploads/2018/11/info-2.jpg" /></a>
			</p>
		</div>
 <?php
	 }else{ ?>
		<div class="top-img-section">
			<p>View web result for <span class="p-key">"<?php echo $search_query = get_search_query(); ?>"</span>, create Image Board & Commission anything<a title="Know how it works" target="_blank" href="/how-it-works"><img src="/wp-content/uploads/2018/11/info-2.jpg" /></a>
			</p>
		</div>
	 <?php
		
		 }
?>
<!--
<div class="top-img-section">
	<p>View web result for <span class="p-key">"<?php echo $search_query = get_search_query(); ?>"</span>, create Image Board & Commission anything<a title="Know how it works" target="_blank" href="/how-it-works"><img src="/wp-content/uploads/2018/11/info-2.jpg" /></a>
	</p>
</div>
-->

			 
<!-- start for dasktop image -->			 
<div class="x-row" id="dasktop_tab_view_img">

<div class="x-column x-sm x-1-1"><a target="_blank" href="https://app.commissionit.com/bingsearch?s=<?php echo $search_query = get_search_query(); ?>"><img src="<?php echo site_url(); ?>/wp-content/uploads/2018/12/art-commissions-image-board.jpg" class="attachment-shop_catalog size-shop_catalog wp-post-image" style="width:236px; height:236px;" /></a>
</div>

</div>
<!-- end for dasktop image -->
<!-- for mobile image -->
<div class="x-row" id="mobile_view_image">

<div class="x-column x-sm x-1-1"><a target="_blank" href="https://app.commissionit.com/bingsearch?s=<?php echo $search_query = get_search_query(); ?>"><img src="<?php echo site_url(); ?>/wp-content/uploads/2018/12/art-commissions-mood-board-.jpg" class="attachment-shop_catalog size-shop_catalog wp-post-image" style="width:236px; height:236px;" /></a>
</div>

</div>
<!-- end for mobile image -->


<div class="web-results">Click <a target="_blank" href="https://app.commissionit.com/bingsearch?s=<?php echo $search_query = get_search_query(); ?>">here</a> to view 20,000 results for <span class="p-key"><?php echo $search_query = get_search_query(); ?></span> </div>

<div class="e16881-129 x-column x-sm x-1-1">
    
<div class="e16881-130 x-text">
  <p><a style="color: #007b69; outline: none; text-align: center; cursor: pointer; font-family: Roboto, 'Helvetica Neue', sans-serif; background-color: #f1f1f1; font-size: 13pt; vertical-align: top;" href="https://privacy.microsoft.com/en-gb/privacystatement" target="_blank" rel="noopener">Microsoft Privacy Statements Powered By<img style="width: 40px; margin-left: 7px;" src="https://app.commissionit.com/assets/images/bingLogo.svg.png" alt="Bing Logo"></a></p></div>
</div>
<div class="web-or">or</div>

              <div class="quote-section">
			 <p>Already Know what you want to commmission & have a specification/ picture and want a quote<a title="Know how it works" target="_blank" href="/how-it-works"><img src="/wp-content/uploads/2018/11/info-2.jpg" /></a></p>
			 </div>
			 <div class="mail-section">
			 <a href="mailto:sales@commissionit.com?subject=<?php echo $search_query = get_search_query(); ?>" title="Mail Us"><img src="<?php echo site_url(); ?>/wp-content/uploads/2018/11/EmailNewsletterGraphic-295x300.png" /></a></div>
			 </div>
			 </div>
		
		<aside class="x-sidebar right" role="complementary">
		<?php dynamic_sidebar( 'main-sidebar' ); ?>
		</aside>
        </main><!-- #main -->
    </section><!-- #primary -->
</div>
<?php //get_sidebar(); ?>
<?php get_footer(); ?>